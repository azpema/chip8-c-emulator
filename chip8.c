#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdbool.h>
#include <time.h>
#include "chip8.h"
bool keyPressed;
bool updateScreen;


void chip8_initialize(){
  
  pc      = 0x200;
  opcode  = 0;
  I       = 0; 
  sp      = 0;

  updateScreen = false;

  // Clear display	
  for (int i=0; i<CHIP8_SCREEN_WIDTH*CHIP8_SCREEN_HEIGHT; i++){
    gfx[i] = (unsigned char) 0;
  }
  // Clear stack
  for (int i=0; i<CHIP8_STACK_SIZE; i++){
   stack[i] = (unsigned char) 0;
  }
  // Clear registers V0-VF
  for (int i=0; i<CHIP8_DATA_REGISTER_COUNT; i++){
    V[i] = (unsigned char) 0;
  }
  // Clear memory
  for (int i=0; i<CHIP8_MEMORY_WORD_COUNT; i++){
    memory[i] = (unsigned char) 0;
  }

  //Reset timers
  delay_timer = 0;
  sound_timer = 0;
  // Load fontset into memory
  for (int i=0; i<CHIP8_FONTSET_SIZE; i++){
    memory[0x50 + i] = chip8_fontset[i];
  }

  srand (time(NULL));
}

void loadGame(const char *file_name){
  struct stat st;
  FILE* gamefile;
  
  gamefile = fopen(file_name, "rb");
  if(gamefile == NULL){
    printf("ERROR: can't open \"%s\" game file\n", file_name);
    return;
  }

  fseek(gamefile, 0, SEEK_END);
  long fsize = ftell(gamefile);
  fseek(gamefile, 0, SEEK_SET);

  char *game = malloc(fsize + 1);
  fread(game, 1, fsize, gamefile);
  fclose(gamefile);

  game[fsize] = 0;

  // Load game into memory
  for(int i=0; i<fsize; i++){
    memory[i + 0x200] = game[i];
    //printf("0x%02x\n", memory[i + 0x200]);
  }
  fclose(gamefile);
}

void chip8_restart(){
  
  pc      = 0x200;
  opcode  = 0;
  I       = 0; 
  sp      = 0;

  updateScreen = false;

  // Clear display	
  for (int i=0; i<CHIP8_SCREEN_WIDTH*CHIP8_SCREEN_HEIGHT; i++){
    gfx[i] = (unsigned char) 0;
  }
  // Clear stack
  for (int i=0; i<CHIP8_STACK_SIZE; i++){
   stack[i] = (unsigned char) 0;
  }
  // Clear registers V0-VF
  for (int i=0; i<CHIP8_DATA_REGISTER_COUNT; i++){
    V[i] = (unsigned char) 0;
  }

  //Reset timers
  delay_timer = 0;
  sound_timer = 0;

}

void emulateCycle(){
  if(delay_timer > 0)
  	delay_timer--;

  if(sound_timer > 0)
  	sound_timer--;
  // Each opcode is composed of 2 bytes. Memory has 1 byte words
  opcode = memory[pc] << 8 | memory[pc+1];
  printf("pc=0x%x op=0x%04x\n", pc, opcode);
  
  /*printf("----------------------------------------------------------------\n");
  for(int i=0; i<CHIP8_SCREEN_HEIGHT; i++){
    for(int j=0; j<CHIP8_SCREEN_WIDTH; j++){
      if(gfx[i*CHIP8_SCREEN_WIDTH + j]==0x1)
        printf("*");
      else
        printf(" ");
    }
    printf("\n");
  }
  printf("----------------------------------------------------------------\n");*/
  switch(opcode & 0xF000){
    case 0x0000:
      switch(opcode & 0xFFFF){
        // Clear the screen
        case 0x00E0:
          for(int i=0; i<CHIP8_SCREEN_WIDTH*CHIP8_SCREEN_HEIGHT; i++){ // 
            gfx[i] = 0x0;
          }
          updateScreen = true;
          pc += 2;
          break;

        // Return from a subroutine
        case 0x00EE:
          sp--;
          pc = stack[sp];
          pc += 2;
          break;

        default: //0x0NNN Execute machine language subroutine at address NNN
          stack[sp] = pc;
          sp++;
          pc = opcode & 0x0FFF;
      }
      break;
     
    case 0x1000: // 0x1NNN: Jumps to address NNN
      pc = opcode & 0x0FFF;
      break;

    case 0x2000: // 0x2NNN: Execute subroutine starting at address NNN
      stack[sp] = pc;
      sp++;
      pc = opcode & 0x0FFF; 
      break;

    case 0x3000: // 0x3XNN: Skip the following instruction if the value of register VX equals NN
      if(V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
        pc += 4;
      else
        pc += 2;
      break;

    case 0x4000: // 0x4XNN: Skip the following instruction if the value of register VX is not equal to NN
      if(V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
        pc += 4;
      else
        pc += 2;
      break;

    case 0x5000: // 0x5XY0: Skip the following instruction if the value of register VX is equal to the value of register VY
      if(V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
        pc += 4;
      else
        pc += 2;
      break;

    case 0x6000: //	0x6XNN: Store number NN in register VX
      V[(opcode & 0x0F00) >> 8] = opcode & 0x00FF;
      pc += 2;
      break;

    case 0x7000: // 0x7XNN:	Add the value NN to register VX
      V[(opcode & 0x0F00) >> 8] += opcode & 0x00FF; 
      pc += 2;
      break;

    case 0x8000: 
      switch(opcode & 0x000F){
        case 0x0000: // 0x8XY0:	Store the value of register VY in register VX
          V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
          pc += 2;
          break;

        case 0x0001: // 0x8XY1:	Set VX to VX OR VY
          V[(opcode & 0x0F00) >> 8] |= V[(opcode & 0x00F0) >> 4];
          pc += 2;
          break;

        case 0x0002: // 0x8XY2:	Set VX to VX AND VY
          V[(opcode & 0x0F00) >> 8] &= V[(opcode & 0x00F0) >> 4];
          pc += 2;
          break;

        case 0x0003: // 0x8XY3:	Set VX to VX XOR VY
          V[(opcode & 0x0F00) >> 8] ^= V[(opcode & 0x00F0) >> 4];
          pc += 2;
          break;

        case 0x0004: // 0x8XY4:	Add the value of register VY to register VX. Set VF to 01 if a carry occurs. Set VF to 00 if a carry does not occur
          if(V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8])) 
            V[0xF] = 1;
          else
            V[0xF] = 0;

          V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
          pc += 2;
          break;

        case 0x0005: // 0x8XY5:	Subtract the value of register VY from register VX. Set VF to 00 if a borrow occurs. Set VF to 01 if a borrow does not occur
          if(V[(opcode & 0x00F0) >> 4] > (V[(opcode & 0x0F00) >> 8])) 
            V[0xF] = 0;
          else
            V[0xF] = 1;

          V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
          pc += 2;
          break;

        case 0x0006: // 0x8XY6:	Store the value of register VY shifted right one bit in register VX. Set register VF to the least significant bit prior to the shift. VY is unchanged
          V[0xF] = V[(opcode & 0x00F0) >> 4] & 0x01;
          V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] >> 1; 
          pc += 2;      
          break;
          
        case 0x0007: // 0x8XY7:	Set register VX to the value of VY minus VX. Set VF to 00 if a borrow occurs. Set VF to 01 if a borrow does not occur
          if(V[(opcode & 0x0F00) >> 8] > (V[(opcode & 0x00F0) >> 4])) 
            V[0xF] = 0;
          else
            V[0xF] = 1;

          V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8];
          pc += 2;
          break;

        case 0x000E: // 0x8XYE:	Store the value of register VY shifted left one bit in register VX. Set register VF to the most significant bit prior to the shift. VY is unchanged
          V[0xF] = V[(opcode & 0x00F0) >> 4] >> 7;
          V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] << 1;
          pc += 2;
          break;
          
        default:
          printf("ERROR: Unknown opcode [0x8000] 0x%x\n", opcode);
      }
    break;

    case 0x9000: //0x9XY0:	Skip the following instruction if the value of register VX is not equal to the value of register VY
      if(V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
        pc += 4;
      else
        pc += 2;
      break;

    case 0xA000: // 0xANNN:	Store memory address NNN in register I
      I = opcode & 0x0FFF;
      pc += 2;
      break;

    case 0xB000: // 0xBNNN:	Jump to address NNN + V0
      pc = (opcode & 0x0FFF) + V[0];
      break;

    case 0xC000: // 0xCXNN:	Set VX to a random number with a mask of NN
      V[(opcode & 0x0F00) >> 8] = (rand() % 0xFF) & (opcode & 0x00FF);
      pc += 2;
      break;

    case 0xD000:{ // 0xDXYN:	Draw a sprite at position VX, VY with N bytes of sprite data starting at the address stored in I. Set VF to 01 if any set pixels are changed to unset, and 00 otherwise
      unsigned char X = V[(opcode & 0x0F00) >> 8]; // VX
      unsigned char Y = V[(opcode & 0x00F0) >> 4]; // VY
      unsigned char N = opcode & 0x000F;           // N
      unsigned char pixel;

      V[0xF] = 0;
      for(int yline=0; yline<N; yline++){
        pixel = memory[I + yline];
        for(int xline=0; xline<CHIP8_PIXEL_WIDTH; xline++){
          if((pixel & (0x80 >> xline)) != 0){
           if (gfx[(X + xline + (Y+yline)*CHIP8_SCREEN_WIDTH)] == 1){
              V[0xF] = 1;
            }
            gfx[(X + xline + (Y+yline)*CHIP8_SCREEN_WIDTH)] ^= 1;
          }
        }
      }
      updateScreen = true;
      pc += 2;
      }
      break;

    case 0xE000:
      switch(opcode & 0x00FF){
        case 0x009E: // 0xEX9E:	Skip the following instruction if the key corresponding to the hex value currently stored in register VX is pressed
          if(key[V[(opcode & 0x0F00) >> 8]] == 0x1)
            pc += 4;
          else 
            pc += 2;
          break;
        
        case 0x00A1: // 0xEXA1 Skip the following instruction if the key corresponding to the hex value currently stored in register VX is not pressed
          if(key[V[(opcode & 0x0F00) >> 8]] == 0x0)
            pc += 4;
          else 
            pc += 2;
          break;

        default:
          printf("ERROR: Unknown opcode [0xE000] 0x%x\n", opcode);
      }
      break;

    case 0xF000:
      switch(opcode & 0x00FF){
        case 0x0007: // 0xFX07:	Store the current value of the delay timer in register VX
          V[(opcode & 0x0F00) >> 8] = delay_timer;
          pc+=2;
          break;

        case 0x000A: // 0xFX0A:	Wait for a keypress and store the result in register VX
          keyPressed = false;
          for(int i=0; i<CHIP8_INPUT_SIZE; i++){
            if(key[i] == 0x1){
              V[(opcode & 0x0F00) >> 8] = i;
              keyPressed=true;
            }
          }

          if(!keyPressed)
            return;

          pc += 2;
          break;

        case 0x0015: // 0xFX15:	Set the delay timer to the value of register VX
          delay_timer = V[(opcode & 0x0F00) >> 8];
          pc += 2;
          break;

        case 0x0018: // 0xFX18:	Set the sound timer to the value of register VX
          sound_timer = V[(opcode & 0x0F00) >> 8];
          pc += 2;
          break;

        case 0x001E:  // 0xFX1E:	Add the value stored in register VX to register I. VF is set to 1 when there is a range overflow (I+VX>0xFFF), and to 0 when there isn't.
          if( I + V[(opcode & 0x0F00) >> 8] > 0xFFF)
            V[0xF] = 1;
          else 
            V[0xF] = 0;
          I += V[(opcode & 0x0F00) >> 8];
          pc += 2;
          break;

        case 0x0029: // 0xFX29: Establece I = VX * largo Sprite Chip-8.
          I = V[(opcode & 0x0F00) >> 8] * 5;
          pc += 2;
          break;

        case 0x0033: // 0xFX33: Guarda la representación de VX en formato humano. Poniendo las centenas en la posición de memoria I, las decenas en I + 1 y las unidades en I + 2  
          V[I + 2] = V[(opcode & 0x0F00) >> 8] % 10;
          V[I + 1] = (V[(opcode & 0x0F00) >> 8]/10) % 10;
          V[I + 0] = (V[(opcode & 0x0F00) >> 8]/100) % 10;
          pc += 2;
          break;

        case 0x0055: // 0xFX55: Store the values of registers V0 to VX inclusive in memory starting at address I. I is set to I + X + 1 after operation
          for(int i=0; i<(opcode & 0x0F00) >> 8; i++){
            memory[I] = V[i];
            I++;
          }
          pc += 2;
          break;

        case 0x0065: // 0xFX65: Fill registers V0 to VX inclusive with the values stored in memory starting at address I. I is set to I + X + 1 after operation
          for(int i=0; i<(opcode & 0x0F00) >> 8; i++){
            V[i] = memory[I];
            I++;
          }
          pc += 2;
          break;
        default:
          printf("ERROR: Unknown opcode [0xF000] 0x%x\n", opcode);
      }
      break;

    default:
      printf("ERROR: Unknown opcode 0x%x\n", opcode);

  }
  //print_registers();

}

void print_registers() {
  for(int i=0; i<CHIP8_DATA_REGISTER_COUNT; i++){
    printf("V%x=%u   ", i, V[i]);
  }
  printf("\n");
  printf("I=%hu\n", I);
  printf("sound_timer=%u           delay_timer=%u", sound_timer, delay_timer);
}

