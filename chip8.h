  #define CHIP8_SCREEN_WIDTH 64
  #define CHIP8_SCREEN_HEIGHT 32
  #define CHIP8_DATA_REGISTER_COUNT 16
  #define CHIP8_MEMORY_WORD_COUNT 4096
  #define CHIP8_STACK_SIZE 16
  #define CHIP8_INPUT_SIZE 16
  #define CHIP8_FONTSET_SIZE 80
  #define CHIP8_PIXEL_WIDTH 8

  unsigned short opcode;
  // 16 1 byte sized CPU registers V0-VF
  unsigned char V[CHIP8_DATA_REGISTER_COUNT];
  // 4096 1 byte sized words
  unsigned char memory[CHIP8_MEMORY_WORD_COUNT];
  // Index register
  unsigned short I;
  unsigned short pc;
  // Graphics. 64 * 32 pixels
  unsigned char gfx[CHIP8_SCREEN_WIDTH * CHIP8_SCREEN_HEIGHT];
  // Timers
  unsigned char delay_timer;
  unsigned char sound_timer;
  // Stack
  unsigned short stack[CHIP8_STACK_SIZE];
  unsigned short sp;
  // Input
  unsigned char key[CHIP8_INPUT_SIZE];

  unsigned char chip8_fontset[CHIP8_FONTSET_SIZE] =
  { 
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
  };

  void chip8_initialize();
  void chip8_restart();
  void emulateCycle();
  void loadGame(const char *file_name);
  void print_registers();