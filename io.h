#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>

#define SCREEN_WIDTH_MULTIPLIER 10
#define SCREEN_HEIGHT_MULTIPLIER 10
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 320

SDL_Window *window;
SDL_Renderer *renderer;

int io_initialize();
int io_updateScreen();
void io_exit();
void io_draw_rectangle();
