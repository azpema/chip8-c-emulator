#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "chip8.c"
#include "io.c"

int main(int argc, char* argv[]){
  int FPS = 50; //Framerate
  chip8_initialize();
  loadGame(argv[1]);
  io_initialize();

  bool quit = false;
  //Event handler
  SDL_Event e;
  struct timespec start, end;
  // Game loop
  while(!quit){
  	clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    Uint32 start_time = SDL_GetTicks();
    //while(SDL_PollEvent( &e )){
      SDL_PollEvent( &e);
      switch (e.type){
        case SDL_QUIT:
          quit = true;
          break;
        case SDL_KEYUP:
          switch(e.key.keysym.sym){
            case SDLK_F1:
                chip8_restart();
              break;
            case SDLK_1:
              key[0x1]=0x0;
              break;
            case SDLK_2:
              key[0x2]=0x0;
              break;
            case SDLK_3:
              key[0x3]=0x0;
              break;
            case SDLK_4:
              key[0xc]=0x0;
              break;
            case SDLK_q:
              key[0x4]=0x0;
              break;
            case SDLK_w:
              key[0x5]=0x0;
              break;
            case SDLK_e:
              key[0x6]=0x0;
              break;
            case SDLK_r:
              key[0xd]=0x0;
              break;
            case SDLK_a:
              key[0x7]=0x0;
              break;
            case SDLK_s:
              key[0x8]=0x0;
              break;
            case SDLK_d:
              key[0x9]=0x0;
              break;
            case SDLK_f:
              key[0xe]=0x0;
              break;
            case SDLK_z:
              key[0xa]=0x0;
              break;
            case SDLK_x:
              key[0x0]=0x0;
              break;
            case SDLK_c:
              key[0xb]=0x0;
              break;
            case SDLK_v:
              key[0xf]=0x0;
              break;
            default:{}
          }
          break;
        case SDL_KEYDOWN:
          switch(e.key.keysym.sym){
            case SDLK_1:
              if(key[0x1] != 0x1 )
                key[0x1]=0x1;
              break;
            case SDLK_2:
              key[0x2]=0x1;
              break;
            case SDLK_3:
              key[0x3]=0x1;
              break;
            case SDLK_4:
              key[0xc]=0x1;
              break;
            case SDLK_q:
              key[0x4]=0x1;
              break;
            case SDLK_w:
              key[0x5]=0x1;
              break;
            case SDLK_e:
              key[0x6]=0x1;
              break;
            case SDLK_r:
              key[0xd]=0x1;
              break;
            case SDLK_a:
              key[0x7]=0x1;
              break;
            case SDLK_s:
              key[0x8]=0x1;
              break;
            case SDLK_d:
              key[0x9]=0x1;
              break;
            case SDLK_f:
              key[0xe]=0x1;
              break;
            case SDLK_z:
              key[0xa]=0x1;
              break;
            case SDLK_x:
              key[0x0]=0x1;
              break;
            case SDLK_c:
              key[0xb]=0x1;
              break;
            case SDLK_v:
              key[0xf]=0x1;
              break;
            default:{}
          }
          break;
        default: {}
      }
    //}
    /*if((1000/FPS)>(SDL_GetTicks()-start_time))
    {
      SDL_Delay((1000/FPS)-(SDL_GetTicks()-start_time)); //Yay stable framerate!
    }*/
    emulateCycle();

    if(updateScreen){
      io_updateScreen();
      updateScreen = false;
    }
    
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000; ;
    //printf("%ld microseconds\n", delta_us);
  }

  io_exit();
  return 0;
}