
#include "io.h"

int io_initialize(){
  
  //Initialize SDL
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
    printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    return EXIT_FAILURE;
  }

  window = SDL_CreateWindow("Chip-8 Emulator", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );

  if (window == NULL){
    printf( "SDL could not create window! SDL_Error: %s\n", SDL_GetError() );
    return EXIT_FAILURE;
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  if (renderer == NULL){
    SDL_DestroyWindow(window);
    printf( "SDL could not create renderer! SDL_Error: %s\n", SDL_GetError() );
    return EXIT_FAILURE;
  }


  return 0;
}

int io_updateScreen(){
  SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
  SDL_RenderClear(renderer);
  for(int x=0; x<CHIP8_SCREEN_WIDTH; x++){
    for(int y=0; y<CHIP8_SCREEN_HEIGHT; y++){
      if(gfx[y*CHIP8_SCREEN_WIDTH+x]==0x1){
        io_draw_rectangle(x, y, 10, 10);
      }
    }
  }
  SDL_RenderPresent(renderer);
  return 0;
}

void io_exit(){
  SDL_DestroyWindow(window);
  SDL_Quit();
}

void io_draw_rectangle(int x, int y, int w, int h){
        SDL_Rect rect;
        rect.x = x * SCREEN_WIDTH_MULTIPLIER;
        rect.y = y * SCREEN_HEIGHT_MULTIPLIER;
        rect.w = 10;
        rect.h = 10;
        SDL_SetRenderDrawColor(renderer, 0xFF,0xFF,0xFF,0xFF);
        SDL_RenderFillRect(renderer, &rect);
}

